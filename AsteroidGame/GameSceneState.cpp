#include "Game.h"
#include "GameStrategy.h"
#include <QMessageBox>
#include <QtMath>

GameSceneState::GameSceneState() {
	Game::Instance().gameScene = this;
	Game::Instance().graphicObjectsdlist = new QVector<GraphicObject*>();
	bulletList = new QVector<Bullet*>();

	player = new Player(350, 250);
	Game::Instance().graphicObjectsdlist->push_back(player);
	connect(Game::Instance().mainWindow, &MainWindow::keyPressed , this, &GameSceneState::changePlayerPosition);

	QTimer *asteroidTimer = new QTimer();
	for (int i = 0; i < 3; i++)
		QObject::connect(asteroidTimer, &QTimer::timeout, this, &GameSceneState::spawnAsteroids);
	asteroidTimer->start(4000);
}

void GameSceneState::paintEvent() {
	QPainter painter(Game::Instance().mainWindow);
	painter.drawPixmap(Game::Instance().mainWindow->rect(), QPixmap(":/MainWindow/Resources/background.jpg"));

	//Score output
	painter.setPen(QPen(Qt::yellow, 50));
	painter.setFont(QFont("Arial", 20, QFont::Bold));
	painter.drawText(-30, 0, 200, 50, Qt::AlignCenter, QString("Score: " + QString(QString::number(score))));

	//Health output
	painter.setPen(QPen(Qt::red, 50));
	painter.setFont(QFont("Arial", 20, QFont::Bold));
	painter.drawText(-30, 50, 200, 50, Qt::AlignCenter, QString("Lives: " + QString::number(player->health)));
	
	//Graphic objects output
	for (int i = 0; i < Game::Instance().graphicObjectsdlist->size(); i++)
		Game::Instance().graphicObjectsdlist->at(i)->show();

	//Bullets output
	for (int i = 0; i < Game::Instance().gameScene->bulletList->size(); i++)
		bulletList->at(i)->show();
	
	//Check health 
	if (player->health <= 0) {
		gameOver();
		return;
	}
		

	backButton = new GameButton(Game::Instance().mainWindow, Game::Instance().mainWindow->width() / 2 - 60, Game::Instance().mainWindow->height() - 140, 120, 120, "Back", 20);
	connect(backButton, &GameButton::clicked, this, [=]() ->void {
		GameClient<MenuStrategy> client;
		client.userStrategy();
		Game::Instance().mainWindow->update();
		delete this;
	});
}

//Handling user control
void GameSceneState::changePlayerPosition() {
		if (Game::Instance().mainWindow->pressedKeys.contains(Qt::Key_A)) {
			player->angle -= 10;
		}
		if (Game::Instance().mainWindow->pressedKeys.contains(Qt::Key_D)) {
			player->angle += 10;
		}
		if (Game::Instance().mainWindow->pressedKeys.contains(Qt::Key_W)) {
			player->acceleration = 20;
		}
		if(Game::Instance().mainWindow->pressedKeys.contains(Qt::Key_Space))
			player->shoot(player->angle);

	Game::Instance().graphicObjectsdlist->at(Game::Instance().graphicObjectsdlist->indexOf(player))->posX = player->posX;
	Game::Instance().graphicObjectsdlist->at(Game::Instance().graphicObjectsdlist->indexOf(player))->posY = player->posY;
	Game::Instance().mainWindow->update();
}

void GameSceneState::spawnAsteroids() {
	srand(time(NULL));
	Asteroid * newAsteroid = new Asteroid(asteroidDirection, 20 + rand() % 30);
	Game::Instance().graphicObjectsdlist->push_back(newAsteroid);
	asteroidDirection = (asteroidDirection + 1) % 4;
}

void GameSceneState::gameOver() {
	std::ofstream fout("Resources\\Score.txt", std::ios_base::app);
	fout << score << std::endl;
	fout.close();

	int reply = QMessageBox::question(nullptr, "Game over",
		"Score:" + QString::number(score) + "\nPlay again?",
		QMessageBox::Yes, QMessageBox::No);
	if (reply == QMessageBox::Yes) {
		GameClient<GameSceneStrategy> client;
		client.userStrategy();
		Game::Instance().mainWindow->update();
		delete this;
	}
	else if (reply == QMessageBox::No) {
		
		GameClient<MenuStrategy> client;
		client.userStrategy();
		Game::Instance().mainWindow->update();
		delete this;
		
	}
}

GameSceneState::~GameSceneState() {
	disconnect(this);
	delete bulletList;
	delete player;
}
