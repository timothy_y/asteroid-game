#include "mainwindow.h"
#include <QtWidgets/QApplication>
#include "Log.h"
#include "Game.h"

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
	Game::Instance().Run();
	return a.exec();
}
