#pragma once
#include <iostream>
#include <sstream>
#include <fstream>
#include <time.h>

class Log
{
public:
	//Operator overload
	template <typename T>
	Log & operator<<(T const & value)
	{
		//Logging only in debug mode
#if _DEBUG
		time_t t = time(NULL);
		*fout << "[" << localtime(&t)->tm_mday << "." << localtime(&t)->tm_mon << "." << localtime(&t)->tm_year << "  " << localtime(&t)->tm_hour << ":" << +localtime(&t)->tm_min << ":" << +localtime(&t)->tm_sec << "]  " << value << std::endl;
#endif
		return *this;
	}
	~Log() {
		fout->close();
	}
private:
	std::ofstream *fout = new std::ofstream("Resources\\GameLog.txt", std::ios_base::app);
};

