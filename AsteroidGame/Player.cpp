#include "Player.h"
#include "Game.h"
#include <qtimer.h>
#include <QtMath>

Player::Player(int x, int y)
{
	posX = x;
	posY = y;

	QTimer *timer = new QTimer();
	connect(timer, &QTimer::timeout, this, &Player::move);
	timer->start(40);
	QTimer *timer2 = new QTimer();
	connect(timer2, &QTimer::timeout, this, [=]()->void {
		recharged = true;
	});
	timer2->start();
}


Player::~Player()
{
}

bool Player::changePos() {
	posY -= (posY > 10 || qCos(qDegreesToRadians(angle)) < 0)
		&& (posY < Game::Instance().mainWindow->height() - 80 || qCos(qDegreesToRadians(angle)) > 0) ? acceleration * qCos(qDegreesToRadians(angle)) : 0;
	posX += (posX > 10 || qSin(qDegreesToRadians(angle)) > 0)
		&& (posX < Game::Instance().mainWindow->width() - 60 || qSin(qDegreesToRadians(angle)) < 0) ? acceleration * qSin(qDegreesToRadians(angle)) : 0;
	if (acceleration)
		acceleration--;
	return true;
}

void Player::shoot(float angle) {
	if (recharged) {
		Game::Instance().gameScene->bulletList->push_back(new Bullet(posX + 10, posY + 10, angle));
		recharged = false;
	}
}

void Player::show() {
	QPainter painter(Game::Instance().mainWindow);
	painter.drawPixmap(posX, posY, QPixmap(":/MainWindow/Resources/SpaceShip.png").transformed(QTransform().rotate(angle)));
}