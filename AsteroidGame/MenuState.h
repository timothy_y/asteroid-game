#pragma once
#include "GameState.h"
#include "Game.h"
#include "Log.h"
class MenuState : public GameState {
	QPixmap* background;
public:
	MenuState();
	void paintEvent();
private:
	GameButton* newGameButton;
	GameButton* statbutton;
	GameButton* exitButton;
};

