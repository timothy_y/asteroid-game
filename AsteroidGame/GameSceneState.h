#pragma once
#include "GameState.h"
#include "Game.h"
#include "Log.h"
#include "Bullet.h"
#include "Asteroid.h"
#include <QLinkedList>
#include "Player.h"


class GameSceneState : public GameState {
	QPixmap* background;
public:
	GameSceneState();
	~GameSceneState();
	void paintEvent();
	QVector<Bullet*>* bulletList;
	GameButton* backButton;
	Player* player;
	int score = 0;
	void changePlayerPosition();
	void spawnAsteroids();
	void gameOver();
	int asteroidDirection;
};

