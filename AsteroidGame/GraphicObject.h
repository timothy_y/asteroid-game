#pragma once 
#include <QObject>
class GraphicObject : public QObject
{
public:
	int posX;
	int posY;

	virtual void detectCollision() = 0;
	virtual void show() = 0;
	virtual bool handleCollision() = 0;
	virtual bool changePos() = 0;

	//Template method
	void move() {
		if (!changePos())
			return;

		detectCollision();
		
		if (!handleCollision())
			return;
		
	}
	QList<GraphicObject*>* collidingItems = new QList<GraphicObject*>();
};

