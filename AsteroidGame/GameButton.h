#pragma once

#include <QObject>
#include <QWidget>
#include <QPainter>

class GameButton : public QObject
{
	Q_OBJECT

public:
	GameButton(QWidget *parent, int x, int y, int width, int haight, QString text, int size);

	void setText(QString str, int size = 10);
	~GameButton();
private:
	QString text;
	QWidget * parent;
	int width;
	int height;
	int x;
	int y;
	int size;
	void paintEvent();
	void mouseReleaseEvent();
signals:
	int clicked();
};
