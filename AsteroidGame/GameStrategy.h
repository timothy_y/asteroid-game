#pragma once
#include <QObject>
#include "Game.h"
#include "GameStrategy.h"
#include "Log.h"
#include "MenuState.h"
#include "LoadingState.h"
#include "GameSceneState.h"
#include "StatisticState.h"

class LoadingStrategy
{
protected:
	void updateWindow() {
		Game::Instance().mainWindow = new MainWindow();
		Game::Instance().mainWindow->setFixedSize(800, 600);
		Game::Instance().mainWindow->setState(new LoadingState());
		Game::Instance().mainWindow->show();
	}
};
class MenuStrategy
{
protected:
	void updateWindow() {

		
		Game::Instance().mainWindow->setState(new MenuState());
		Game::Instance().mainWindow->update();
		
	}
};

class GameSceneStrategy
{
protected:
	void updateWindow() {
		Game::Instance().mainWindow->setState(new GameSceneState());
		Game::Instance().mainWindow->update();
	}
};
class StatisticStrategy
{
protected:
	void updateWindow() {
		Game::Instance().mainWindow->setState(new StatisticState());
		Game::Instance().mainWindow->update();
	}
};

//Strategy client for opening windows
//Param "Strategy" - window strategy(GameSceneStrategy, ResultTableStrategy, MainWindowStrategy)
template<class Strategy>
class GameClient : public Strategy {
public:
	void userStrategy() {
		this->updateWindow();
	}
};
