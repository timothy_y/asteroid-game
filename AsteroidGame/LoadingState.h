#pragma once
#include "GameState.h"
#include "Game.h"
#include "Log.h"
class LoadingState : public GameState {
private:
	int time = 0;
	int frameCounter = 1;
	int lineCounter = 0;
	void nextAnimationFrame();
	QTimer* timer;
	GameButton* newGameButton;
	GameButton* statbutton;
	GameButton* exitButton;
	QPixmap* background;
public:
	LoadingState();
	void paintEvent();
};