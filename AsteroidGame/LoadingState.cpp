#include "LoadingState.h"
#include "GameStrategy.h"


LoadingState::LoadingState() {
	timer = new QTimer();
	connect(timer, &QTimer::timeout, this, &LoadingState::nextAnimationFrame);
	if (time == 0) {
		timer->start(0);
	}
}

void LoadingState::paintEvent() {
	QPainter painter(Game::Instance().mainWindow);

	painter.setRenderHint(QPainter::Antialiasing, true);
	painter.drawPixmap(Game::Instance().mainWindow->rect(), QPixmap(":/MainWindow/Resources/background.jpg"));

	painter.setPen(QPen(Qt::green, 60, Qt::SolidLine));
	if (lineCounter == 0)
		painter.drawPixmap(frameCounter, Game::Instance().mainWindow->height() - 50, QPixmap(":/MainWindow/Resources/SpaceShip2.png"));
	if (frameCounter == Game::Instance().mainWindow->width())
		lineCounter = 1;
	if (lineCounter == 1)
	{
		GameClient<MenuStrategy> client;
		client.userStrategy();
	}
}

void LoadingState::nextAnimationFrame() {
	frameCounter++;
	Game::Instance().mainWindow->update();
}
