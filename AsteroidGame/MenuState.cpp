#include "MenuState.h"
#include "GameStrategy.h"
#include "Game.h"

MenuState::MenuState() {

}
void MenuState::paintEvent() {

	QPainter painter(Game::Instance().mainWindow);

	painter.setRenderHint(QPainter::Antialiasing, true);
	painter.drawPixmap(Game::Instance().mainWindow->rect(), QPixmap(":/MainWindow/Resources/background.jpg"));

	newGameButton = new GameButton(Game::Instance().mainWindow, Game::Instance().mainWindow->width() / 2 - 60, 20, 120, 120, "New\nGame", 20);
	connect(newGameButton, &GameButton::clicked, this, [=]() ->void {

		//Open window
		GameClient<GameSceneStrategy> client;
		client.userStrategy();
		//Logging
		Log() << "newGameButton was clicked -> gameSceneWindow opened";
		delete this;
	});
	statbutton = new GameButton(Game::Instance().mainWindow, Game::Instance().mainWindow->width() / 2 - 60, Game::Instance().mainWindow->height() / 2 - 60, 120, 120, "Statistic", 18);
	Game::Instance().mainWindow->connect(statbutton, &GameButton::clicked, this, [=]() ->void {
		//Open window
		GameClient<StatisticStrategy> client;
		client.userStrategy();
		////Logging
		Log() << "resultTableButton was clicked -> resultTableWindow opened";
		delete this;
	});
	exitButton = new GameButton(Game::Instance().mainWindow, Game::Instance().mainWindow->width() / 2 - 60, Game::Instance().mainWindow->height() - 140, 120, 120, "Exit", 20);
	connect(exitButton, &GameButton::clicked, this, [=]() ->void {
		exit(0);
		//Logging
		Log() << "exitButton was clicked -> exit game";
		delete this;
	});
}