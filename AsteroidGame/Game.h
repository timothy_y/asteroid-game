#pragma once
#include <QVector>
#include <iostream>
#include "MainWindow.h"
#include "GraphicObject.h"
#include "GameSceneState.h"

class Game
{
public:
	static Game& Instance()
	{
		static Game theSingleInstance;
		return theSingleInstance;
	}
	static void Run();

	MainWindow * mainWindow;
	QVector<GraphicObject*>* graphicObjectsdlist = new QVector<GraphicObject*>();
	GameSceneState* gameScene;
	int windowWidth = 800;
	int windowHeight = 800;
	int playerSpeed = 20;
	int halfPlayerSize = 20;
	int spawnTime = 3000;
	int enemiesPerSpawn = 4;


private:
	Game() {};
	Game(const Game& root);
	Game& operator=(const Game&);

};

