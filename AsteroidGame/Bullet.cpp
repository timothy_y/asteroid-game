#include "Bullet.h"
#include <qtimer.h>
#include <Game.h>
#include <QtMath>

Bullet::Bullet(int x, int y, float angle)
{
	posX = x; 
	posY = y;

	dx = 0, dy = 0;
	dx = -10 * qCos(qDegreesToRadians(angle));
	dy = 10 * qSin(qDegreesToRadians(angle));

	timer = new QTimer();	
	connect(timer, &QTimer::timeout, this, &Player::move);
	timer->start(10);
	
}


Bullet::~Bullet()
{
	timer->stop();
	disconnect(this);
	delete timer;
	delete collidingItems;
}

void Bullet::detectCollision()
{
	
	for (int i = 0; i < Game::Instance().graphicObjectsdlist->size(); i++)
		if (this != Game::Instance().graphicObjectsdlist->at(i)) {
			if (this->posX <= Game::Instance().graphicObjectsdlist->at(i)->posX + static_cast<Asteroid*>(Game::Instance().graphicObjectsdlist->at(i))->enemyLevel  &&
				this->posX >= Game::Instance().graphicObjectsdlist->at(i)->posX  &&
				this->posY <= Game::Instance().graphicObjectsdlist->at(i)->posY + static_cast<Asteroid*>(Game::Instance().graphicObjectsdlist->at(i))->enemyLevel &&
				this->posY >= Game::Instance().graphicObjectsdlist->at(i)->posY ) {
				collidingItems->push_back(Game::Instance().graphicObjectsdlist->at(i));
			}
		}
		
}
bool Bullet::handleCollision() {
	for (int i = 0; i < collidingItems->size(); i++) {
		if (typeid(*collidingItems->at(i)) == typeid(Asteroid)) {
			if (static_cast<Asteroid*>(collidingItems->at(i))->enemyLevel > 20) {
				Game::Instance().gameScene->bulletList->removeOne(this);
				Game::Instance().graphicObjectsdlist->removeOne(collidingItems->at(i));

				Game::Instance().gameScene->score++;

				Asteroid *newAsteroid = new Asteroid(0, static_cast<Asteroid*>(collidingItems->at(i))->enemyLevel - 10);
				newAsteroid->posX = static_cast<Asteroid*>(collidingItems->at(i))->posX;
				newAsteroid->posY = static_cast<Asteroid*>(collidingItems->at(i))->posY;
				Game::Instance().graphicObjectsdlist->push_back(newAsteroid);
				delete collidingItems->at(i);
				delete this;
			}
			else {
				Game::Instance().gameScene->bulletList->removeOne(this);
				Game::Instance().graphicObjectsdlist->removeOne(collidingItems->at(i));
				Game::Instance().gameScene->score++;
				delete collidingItems->at(i);
				delete this;
			}
			return false;

		}
	}
	return true;
}

bool Bullet::changePos() {
	if (posX > Game::Instance().mainWindow->width() || posX < 0 || posY > Game::Instance().mainWindow->height() || posY < 0) {
		Game::Instance().gameScene->bulletList->removeOne(this);
		delete this;
		return false;
	}

	posX += dy; posY += dx;
	return true;
}

void Bullet::show() {
	QPainter painter(Game::Instance().mainWindow);
	painter.setPen(Qt::black);
	painter.setBrush(Qt::yellow);
	painter.drawEllipse(posX, posY, 5, 5);
}