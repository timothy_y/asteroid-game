#pragma once
#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QtWidgets/QWidget>
#include "GameState.h"
#include <QKeyEvent>

class MainWindow : public QWidget
{
	Q_OBJECT

	class GameState *currentState;
public:
	MainWindow(QWidget *parent = 0);
	~MainWindow();
	void setState(GameState * state);
	float wheelPos = 0;
	bool wheelDown;
	bool scrollPossibility = false;
	QSet<int> pressedKeys;
protected:
	void paintEvent(QPaintEvent* event) override;
	void mouseReleaseEvent(QMouseEvent * event);
	void keyPressEvent(QKeyEvent *event);
	void keyReleaseEvent(QKeyEvent *event);
	void wheelEvent(QWheelEvent *event);
signals:
	int clicked();
	int keyPressed();
	int scrolled();

};

#endif // MAINWINDOW_H
