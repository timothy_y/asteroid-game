#pragma once
#include <QObject>
#include <QVector>
class StatSort
{
public:
	StatSort(QVector<int>* vector);
	~StatSort();
	void quickSortAlg(int left, int right);
private:
	QVector<int> * tempVector = new QVector<int>();
};

