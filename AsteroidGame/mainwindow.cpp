#include "MainWindow.h"
#include "Game.h"
#include <QKeyEvent>

MainWindow::MainWindow(QWidget *parent) : QWidget(parent)
{
	
}

MainWindow::~MainWindow()
{

}

void MainWindow::paintEvent(QPaintEvent * event)
{
	currentState->paintEvent();
}
void MainWindow::setState(GameState* state)
{
	currentState = state;
}

void MainWindow::mouseReleaseEvent(QMouseEvent * event) {
	emit clicked();
}
void MainWindow::wheelEvent(QWheelEvent * event) {
	if(scrollPossibility)
		wheelPos += event->delta() / 10;
	wheelDown = event->delta() < 0 ? true : false;
	emit scrolled();
}

void MainWindow::keyPressEvent(QKeyEvent* event) {
	pressedKeys += event->key();
	emit keyPressed();
	
}

void MainWindow::keyReleaseEvent(QKeyEvent* event) {
	pressedKeys -= event->key();
	//emit keyPressed();
}