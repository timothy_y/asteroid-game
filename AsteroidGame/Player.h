#pragma once
#include "GraphicObject.h"
#include "Bullet.h"

class Player : public GraphicObject
{
public:
	Player(int x, int y);
	~Player();
	void shoot(float angle);
	int health = 3;
	float angle = 0;
	float acceleration = 0;
	void detectCollision() override {}
	bool changePos() override;
	bool handleCollision() override { return true; }
	void show() override;
	bool recharged = true;
};

