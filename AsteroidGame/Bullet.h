#pragma once
#include "GraphicObject.h"
#include <QTimer>

enum bulletDirection {
	up,
	down,
	right,
	left
};

class Bullet : public GraphicObject
{
	//Q_OBJECT
public:
	Bullet(int x, int y, float angle);
	~Bullet();
	void detectCollision() override;
	void show() override;
	bool changePos() override;
	bool handleCollision() override;
	int dx, dy;
private:
	QTimer *timer;

	//QList<GraphicObject*>* collidingItems = new QList<GraphicObject*>();


};

