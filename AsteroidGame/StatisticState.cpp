#include "StatisticState.h"
#include "GameStrategy.h"
#include "Game.h"
#include "StatSort.h"


StatisticState::StatisticState() {

	std::ifstream fin("Resources\\Score.txt", std::ios_base::in);
	std::string str;

	//Reading data from file
	while (std::getline(fin, str)) {
		scoreVector->push_back(atoi(str.c_str()));
	}
	fin.close();

	//Sorting data
	StatSort sort(scoreVector);

	connect(Game::Instance().mainWindow, &MainWindow::scrolled, this, &StatisticState::scroll);
}
void StatisticState::paintEvent() {
	QPainter painter(Game::Instance().mainWindow);

	painter.drawPixmap(Game::Instance().mainWindow->rect(), QPixmap(":/MainWindow/Resources/background.jpg"));
	painter.setPen(QPen(Qt::yellow, 50));
	painter.setFont(QFont("Arial", 20, QFont::Bold));

	//Statistic list output
	for (int i = 0; i < scoreVector->size(); i++)
		painter.drawText(0, 50 * i + scrollPos, 800, 50, Qt::AlignCenter, QString(QString::number(scoreVector->at(scoreVector->size() - i - 1))));

	GameButton* backButton = new GameButton(Game::Instance().mainWindow, Game::Instance().mainWindow->width() - 140, Game::Instance().mainWindow->height() - 140, 120, 120, "Back", 20);
	connect(backButton, &GameButton::clicked, this, []() ->void {
		GameClient<MenuStrategy> client;
		client.userStrategy();
		Game::Instance().mainWindow->update();
	});
}

//Handling scroll
void StatisticState::scroll() {
	if (scoreVector->size() * 50 > 600) {
		Game::Instance().mainWindow->scrollPossibility = true;
		if(Game::Instance().mainWindow->wheelDown && -Game::Instance().mainWindow->wheelPos < scoreVector->size() * 50 - 600)
			scrollPos = Game::Instance().mainWindow->wheelPos;
		else if (!Game::Instance().mainWindow->wheelDown && Game::Instance().mainWindow->wheelPos < 0)
			scrollPos = Game::Instance().mainWindow->wheelPos;
		else Game::Instance().mainWindow->scrollPossibility = false;
		
	}
	else Game::Instance().mainWindow->scrollPossibility = false;
		
	Game::Instance().mainWindow->update();
}