#include "StatSort.h"

StatSort::StatSort(QVector<int>* vector)
{
	this->tempVector = vector;
	quickSortAlg(0, tempVector->size() - 1);
	vector = tempVector;
}

StatSort::~StatSort()
{
}

void StatSort::quickSortAlg(int left, int right)
{
	int i = left, j = right;
	int temp;
	int pivot = tempVector->at((left + right) / 2);
	do {
		while (tempVector->at(i) < pivot) i++;
		while (tempVector->at(j) > pivot) j--;

		if (i <= j) {
			std::iter_swap(tempVector->begin() + i, tempVector->begin() + j);
			i++; j--;
		}
	} while (i <= j);

	if (j > left) quickSortAlg(left, j);
	if (right > i) quickSortAlg(i, right);
}
