#pragma once
#include <qwidget.h>
#include <QObject>
#include <QWidget>
#include <QPainter>
#include <QTimer>
#include <QPixmap>
#include "GameButton.h"


class GameState : public QObject
{
public:
	virtual void paintEvent() = 0;
};
