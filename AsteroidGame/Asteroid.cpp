#include "Asteroid.h"
#include <qtimer.h>
#include "Game.h"
#include "time.h"



Asteroid::Asteroid( int dir, int level)
{
	direction = dir;

	enemyLevel = level < maxLevel ? level : maxLevel;
	

	srand(time(NULL));

	if (direction == 0) {
		posX = rand() % Game::Instance().mainWindow->width() - enemyRadius; posY = 0;
	}
		
	else if (direction == 1) {
		posX = Game::Instance().mainWindow->width(); posY = rand() % Game::Instance().mainWindow->height() - enemyRadius;
	}
		
	else if (direction == 2) {
		posX = rand() % Game::Instance().mainWindow->width() - enemyRadius; posY =  Game::Instance().mainWindow->height();
	}
		
	else if (direction == 3) {
		posX = 0; posY = rand() % Game::Instance().mainWindow->width() - enemyRadius;
	}
	currentAcceleration = rand() % maxAcceleration;
	if (rand() % 2 == 0)
		speed = -currentAcceleration;
	else speed = currentAcceleration;
		
	QTimer *timer = new QTimer();
	connect(timer, &QTimer::timeout, this, &Player::move);
	timer->start(50);
	
}

bool Asteroid::handleCollision()
{

	for (int i = 0; i < collidingItems->size(); i++) {
		if (typeid(*collidingItems->at(i)) == typeid(Player)) {
			Game::Instance().gameScene->player->health--;
			
			Game::Instance().graphicObjectsdlist->removeOne(this);
			delete this;
			return false;
		}

		if (typeid(*collidingItems->at(i)) == typeid(Asteroid)) {		
			Game::Instance().graphicObjectsdlist->removeOne(collidingItems->at(i));
			Game::Instance().graphicObjectsdlist->removeOne(this);
			Asteroid *newEnemy = new Asteroid(rand() % 3, static_cast<Asteroid*>(collidingItems->at(i))->enemyLevel + enemyLevel);
			newEnemy->posX = posX;
			newEnemy->posY = posY;
				
			Game::Instance().graphicObjectsdlist->push_back(newEnemy);

			delete collidingItems->at(i);
			delete this;
			return false;
		}
	}

	
	return true;
	
}

bool Asteroid::changePos() {
	if (direction == 0)
		setPos(posX + speed, posY + enemySpeed);
	else if (direction == 1)
		setPos(posX - enemySpeed, posY + speed);
	else if (direction == 2)
		setPos(posX + speed, posY - enemySpeed);
	else if (direction == 3)
		setPos(posX + enemySpeed, posY + speed);
	else if (direction == 4)
		setPos(posX + enemySpeed, posY + enemySpeed);
	else if (direction == 5)
		setPos(posX - enemySpeed, posY - enemySpeed);


	if (posY > Game::Instance().mainWindow->height()) {
		direction = 2;
		setPos(posX, posY - enemySpeed * 2);
	}
	if (posY < 0) {
		direction = 0;
		setPos(posX, posY + enemySpeed * 2);

	}
	if (posX > Game::Instance().mainWindow->width()) {
		direction = 1;
		setPos(posX - enemySpeed * 2, posY);
	}
	if (posX < 0) {
		direction = 3;
		setPos(posX + enemySpeed * 2, posY);
	}
	if (posY > Game::Instance().mainWindow->height() - 25 && posX > Game::Instance().mainWindow->width() - 25)
	{
		direction = 5;
		setPos(Game::Instance().mainWindow->height() - 100, Game::Instance().mainWindow->width() - 100);
	}
	if (posY <  25 && posX < 25)
	{
		direction = 4;
		setPos(100, 100);
	}
	return true;
}

Asteroid::~Asteroid()
{
	disconnect(this);
	delete collidingItems;
}

void Asteroid::detectCollision()
{
	for (int i = 0; i < Game::Instance().graphicObjectsdlist->size(); i++)
		if (this != Game::Instance().graphicObjectsdlist->at(i)) {
			if (this->posX <= Game::Instance().graphicObjectsdlist->at(i)->posX + enemyLevel  &&
				this->posX >= Game::Instance().graphicObjectsdlist->at(i)->posX - enemyLevel  &&
				this->posY <= Game::Instance().graphicObjectsdlist->at(i)->posY + enemyLevel &&
				this->posY >= Game::Instance().graphicObjectsdlist->at(i)->posY - enemyLevel) {
				collidingItems->push_back(Game::Instance().graphicObjectsdlist->at(i));
			}
		}
			
}

void Asteroid::show() {
	QPainter painter(Game::Instance().mainWindow);
	painter.setPen(Qt::black);
	painter.setBrush(QImage(":/MainWindow/Resources/texture.png"));
	painter.drawEllipse(posX, posY, enemyLevel, enemyLevel);
}
			

