#pragma once
#include "GameState.h"
#include "Game.h"
#include "Log.h"
#include <QVector>
class StatisticState : public GameState
{
	QPixmap* background;

public:
	StatisticState();
	void paintEvent();
	QVector<int> * scoreVector = new QVector<int>();
	float scrollPos = 0;
	void scroll();
};

