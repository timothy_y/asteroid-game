#include "GameButton.h"
#include "mainwindow.h"
#include "Game.h"

GameButton::GameButton(QWidget *parent, int x, int y, int width, int height, QString text, int size)
{
	this->parent = parent;
	this->width = width;
	this->height = height;
	this->x = x;
	this->y = y;
	this->text = text;
	this->size = size;
	paintEvent();
	connect(Game::Instance().mainWindow, &MainWindow::clicked, this, &GameButton::mouseReleaseEvent);
}

GameButton::~GameButton()
{

}

void GameButton::paintEvent()
{
	QPainter painter(parent);
	painter.setBrush(QPixmap(":/MainWindow/Resources/texture.png"));
	painter.drawEllipse(x, y, width, height);
	painter.setPen(QPen(Qt::yellow, 50));
	painter.setFont(QFont("Arial", size, QFont::Bold));
	painter.drawText(x, y, width, height, Qt::AlignCenter, text);
}

void GameButton::mouseReleaseEvent()
{
	QPoint pos = parent->mapFromGlobal(QCursor::pos());
	if (pos.x() >= x && pos.x() <= x + width && pos.y() >= y && pos.y() <= y + height)
		emit clicked();
	
}

void GameButton::setText(QString str, int size) {
	text = str;
	this->size = size;
}
