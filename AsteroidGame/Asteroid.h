#pragma once
#include "GraphicObject.h"

class Asteroid : public GraphicObject
{
	//Q_OBJECT
public:
	Asteroid(int dir, int level);
	int direction;
	int currentAcceleration;
	int speed;
	int enemyLevel;
	int enemyRadius = 30;
	int maxAcceleration = 10;
	int enemySpeed = 3;
	const int maxLevel = 100;
	void detectCollision() override;
	void show() override;
	bool handleCollision() override;
	bool changePos() override;
	void setPos(int x, int y) {
		posX = x; posY = y;
	}
	~Asteroid();
};

